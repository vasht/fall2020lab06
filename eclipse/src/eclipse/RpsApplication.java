//Vashti Lanz Rubio 1931765
package eclipse;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsApplication extends Application{
	private RpsGame game;
	public void start(Stage stage) {
		game = new RpsGame();
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
		
		VBox overall = new VBox();
		
		HBox buttons = new HBox();
		Button b1 = new Button("rock");
		Button b2 = new Button("paper");
		Button b3 = new Button("scissor");
		buttons.getChildren().addAll(b1,b2,b3);
		
		HBox textfields = new HBox();
		TextField tf1 = new TextField("Welcome!");
		TextField tf2 = new TextField("wins: " + game.getWins());
		TextField tf3 = new TextField("losses: " + game.getLoses());
		TextField tf4 = new TextField("ties: " + game.getTies());
		tf1.setPrefWidth(200);
		tf2.setPrefWidth(200);
		tf3.setPrefWidth(200);
		tf4.setPrefWidth(200);
		textfields.getChildren().addAll(tf1,tf2,tf3,tf4);
		
		RpsChoice b1_game_handler = new RpsChoice(tf1,tf2,tf3,tf4,b1.getText(),game);
		RpsChoice b2_game_handler = new RpsChoice(tf1,tf2,tf3,tf4,b2.getText(),game);
		RpsChoice b3_game_handler = new RpsChoice(tf1,tf2,tf3,tf4,b3.getText(),game);
		b1.setOnAction(b1_game_handler);
		b2.setOnAction(b2_game_handler);
		b3.setOnAction(b3_game_handler);
		
		overall.getChildren().addAll(buttons,textfields);
		root.getChildren().add(overall);
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }

}
