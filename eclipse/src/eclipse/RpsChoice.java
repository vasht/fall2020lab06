//Vashti Lanz Rubio 1931765
package eclipse;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField tf1;
	private TextField tf2;
	private TextField tf3;
	private TextField tf4;
	private String p_choice;
	private RpsGame game;
	
	public RpsChoice(TextField tf1, TextField tf2, TextField tf3, TextField tf4, String p_choice, RpsGame game) {
		this.tf1 = tf1;
		this.tf2 = tf2;
		this.tf3 = tf3;
		this.tf4 = tf4;
		this.p_choice = p_choice;
		this.game = game;
	}
	public void handle(ActionEvent e) {
		String result = game.playRound(p_choice);
		tf1.setText(result);
		tf2.setText("wins: " + game.getWins());
		tf3.setText("losses: " + game.getLoses()); 
		tf4.setText("ties: " + game.getTies());

	}
}
