//Vashti Lanz Rubio 1931765
package eclipse;

import java.util.Random;

public class RpsGame {
	private int wins;
	private int loses;
	private int ties;
	Random r;
	
	public RpsGame() {
		wins = 0;
		loses = 0;
		ties = 0;
		r = new Random();
	}
	public int getWins() {
		return wins;
	}
	public int getLoses() {
		return loses;
	}
	public int getTies() {
		return ties;
	}
	public String playRound(String p_choice) {
		String[] choice_pool = {"rock","paper","scissor"};
		String cpu_choice = choice_pool[r.nextInt(3)];
		String msg = "Computer chooses " + cpu_choice + ". ";
		
		String result = "";
		if(p_choice.equals(choice_pool[0])) {
			result = playRock(cpu_choice);
		}
		if(p_choice.equals(choice_pool[1])) {
			result = playPaper(cpu_choice);
		}
		if(p_choice.equals(choice_pool[2])) {
			result = playScissor(cpu_choice);
		}
		return msg + result;
	}
	private String playRock(String cpu_choice) {
		String result = "";
		if(cpu_choice.equals("rock")) {
			result =  "Tie.";
			ties++;
		}
		if(cpu_choice.equals("paper")) {
			result =  "You lose.";
			loses++;
		}
		if(cpu_choice.equals("scissor")) {
			result =  "You win.";
			wins++;
		}
		return result;
	}
	private String playPaper(String cpu_choice) {
		String result = "";
		if(cpu_choice.equals("rock")) {
			result =  "You win.";
			wins++;
		}
		if(cpu_choice.equals("paper")) {
			result =  "Tie.";
			ties++;
		}
		if(cpu_choice.equals("scissor")) {
			result =  "You lose.";
			loses++;
		}
		return result;
	}
	private String playScissor(String cpu_choice) {
		String result = "";
		if(cpu_choice.equals("rock")) {
			result =  "You lose.";
			loses++;
		}
		if(cpu_choice.equals("paper")) {
			result =  "You win.";
			wins++;
		}
		if(cpu_choice.equals("scissor")) {
			result =  "Tie.";
			ties++;
		}
		return result;
	}
}

